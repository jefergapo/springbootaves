package com.app.aves.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import com.app.aves.model.Ave;
import com.app.aves.model.Pais;
import com.app.aves.model.Zona;
import com.app.aves.repository.AveRepository;
import com.app.aves.repository.PaisRepository;
import com.app.aves.repository.ZonaRepository;



@CrossOrigin(origins = { "http://localhost:4200" }, maxAge = 3000)
@RestController
@RequestMapping("/api")
public class AveController {
	
	
	private AveRepository aveRepository;
	private ZonaRepository zonaRepository;
	private PaisRepository paisRepository;
	
	@Autowired
	public AveController(AveRepository aveRepository, ZonaRepository zonaRepository, PaisRepository paisRepository) {
		this.aveRepository = aveRepository;
		this.zonaRepository = zonaRepository;
		this.paisRepository = paisRepository;
	}
	
	@GetMapping("/aves")
	public List<Ave> getAll(){
		return aveRepository.findAll();
	}
	
	@PostMapping("/aves")
	public Ave createAve(@RequestBody Ave ave) {
		return aveRepository.save(ave);
	}
	
	@GetMapping("/aves/{codigo}")
	public Ave getAveByCodigo(@PathVariable(value = "codigo") String codigo) {
		return aveRepository.findByCodigo(codigo);
	}
	
	@PutMapping("/aves/{codigo}")
	public Ave updateAve(@PathVariable(value = "codigo") String codigo, @RequestBody Ave currentAve) {

		Ave ave = aveRepository.findByCodigo(codigo);

		ave.setCodigo(currentAve.getCodigo());
		ave.setNombreComun(currentAve.getNombreComun());
		ave.setNombreCientifico(currentAve.getNombreCientifico());

		Ave updatedAve = aveRepository.save(ave);
		return updatedAve;
	}
	
	@DeleteMapping("/aves/{codigo}")
	public ResponseEntity<?> deleteAve(@PathVariable(value = "codigo") String codigo) {
		Ave ave = aveRepository.findByCodigo(codigo);

		aveRepository.delete(ave);

		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/aves/buscar")
	public List<Ave> findByZonaAndNombre(@RequestParam(value = "nombre", required = false) String nombre, @RequestParam String zona){
		System.out.println(zona + "," + nombre);
		
		if (nombre == null || nombre == "" ) return aveRepository.findDistinctAveByPaisesZonaNombre(zona);
		
		return aveRepository.findDistinctAveByNombreComunLikeOrNombreCientificoLikeAndPaisesZonaNombre("%" + nombre + "%", "%" + nombre + "%", zona);
		
		
	}
	
	@GetMapping("/aves/paises")
	public List<Pais> getAllPaises(){
		return paisRepository.findAll();
	}
	
	@GetMapping("/aves/paises/zonas")
	public List<Zona> getAllZona(){
		return zonaRepository.findAll();
	}
	
	
	
	

}