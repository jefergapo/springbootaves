package com.app.aves.model;

import com.app.aves.model.*;

import lombok.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


@Entity
@Table(name = "TONT_PAISES")
@Getter @Setter
@NoArgsConstructor
@ToString @EqualsAndHashCode
public class Pais implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CDPAIS" , unique=true, columnDefinition="VARCHAR(3)")
    private String codigo;
	
	@Column(name = "DSNOMBRE")
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name = "CDZONA")
    private Zona zona;
	
    
}