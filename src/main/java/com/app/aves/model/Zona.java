package com.app.aves.model;

import com.app.aves.model.*;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "TONT_ZONAS")
@Getter @Setter
@NoArgsConstructor
@ToString @EqualsAndHashCode
public class Zona implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CDZONA" , unique=true, columnDefinition="VARCHAR(3)")
    private String codigo;
	
	@Column(name = "DSNOMBRE")
	private String nombre;
}