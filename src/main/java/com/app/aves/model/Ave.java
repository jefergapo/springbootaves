package com.app.aves.model;

import com.app.aves.model.*;

import lombok.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


@Entity
@Table(name = "TONT_AVES")
@Getter @Setter
@NoArgsConstructor
@ToString @EqualsAndHashCode
public class Ave implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CDAVE", unique=true, columnDefinition="VARCHAR(5)")
    private String codigo;
	
	@Column(name = "DSNOMBRE_COMUN")
	private String nombreComun;
	
	@Column(name = "DSNOMBRE_CIENTIFICO")
	private String nombreCientifico;
	
	@ManyToMany
    @JoinTable(name = "TONT_AVES_PAIS",
               joinColumns = @JoinColumn(name="AVECD", referencedColumnName="CDAVE"),
               inverseJoinColumns = @JoinColumn(name="PAISCD", referencedColumnName="CDPAIS"))
	private Set<Pais> paises = new HashSet<>();
}
