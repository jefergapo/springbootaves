package com.app.aves.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.aves.model.Zona;



public interface ZonaRepository extends JpaRepository<Zona, String>{

	
}
