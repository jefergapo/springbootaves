package com.app.aves.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.aves.model.Pais;



public interface PaisRepository extends JpaRepository<Pais, String>{

	
}
