package com.app.aves.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.aves.model.Ave;



public interface AveRepository extends JpaRepository<Ave, String>{

	Ave findByCodigo(String codigo);
	
	List findDistinctAveByNombreComunLikeOrNombreCientificoLikeAndPaisesZonaNombre(String nombreComun, String nombreCientifico, String Nombre);
	
	List findDistinctAveByPaisesZonaNombre(String Nombre);
	
}
